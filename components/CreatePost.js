import { useState } from "react";
import fire from "../config/fire-config";

export default function CreatePost() {
   const [title, setTitle] = useState("");
   const [content, setContent] = useState("");
   const [notification, setNotification] = useState("");

   function handleSubmit(event) {
      event.preventDefault();

      fire.firestore().collection("blog").add({
         title,
         content,
      });

      setTitle("");
      setContent("");

      setNotification("Blogspot created");

      setTimeout(() => {
         setNotification("");
      }, 2000);
   }

   return (
      <div>
         <h2>Add Blog</h2>

         {notification}

         <form onSubmit={handleSubmit}>
            <div>
               Title <br />
               <input
                  type="text"
                  value={title}
                  onChange={({ target }) => setTitle(target.value)}
               />
            </div>

            <div>
               Content <br />
               <textarea
                  value={content}
                  onChange={({ target }) => setContent(target.value)}
               ></textarea>
            </div>

            <button>submit</button>
         </form>
      </div>
   );
}
