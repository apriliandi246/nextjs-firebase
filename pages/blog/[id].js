import fire from "../../config/fire-config";
import Link from "next/link";

export default function Blog({ title, content }) {
   return (
      <div>
         <h2>{title}</h2>
         <p>{content}</p>
         <Link href="/">
            <a>Back</a>
         </Link>
      </div>
   );
}

export async function getServerSideProps({ query }) {
   const content = {};

   await fire
      .firestore()
      .collection("blog")
      .doc(query.id)
      .get()
      .then((result) => {
         content["title"] = result.data().title;
         content["content"] = result.data().content;
      });

   return {
      props: JSON.parse(JSON.stringify(content)),
   };
}
