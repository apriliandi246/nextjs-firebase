import firebase from "firebase/app";
import "firebase/firebase-firestore";
import "firebase/firebase-auth";

const firebaseConfig = {
   apiKey: "AIzaSyAGSlnNMd6Xy7mMrZF5DdSJW2HwxAHsFxU",
   authDomain: "nextjs-firebase-1a043.firebaseapp.com",
   projectId: "nextjs-firebase-1a043",
   storageBucket: "nextjs-firebase-1a043.appspot.com",
   messagingSenderId: "156414198150",
   appId: "1:156414198150:web:e833caf3ff4fec13e1ac07",
   measurementId: "G-ZE02J4NPRW",
};

try {
   firebase.initializeApp(firebaseConfig);
} catch (err) {
   if (!/already exists/.test(err.message)) {
      console.error("Firebase initialization error", err.stack);
   }
}

const fire = firebase;
export default fire;
